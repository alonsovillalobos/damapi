/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('plataforma', {
    plataforma_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(integra.plataforma_plataforma_id_seq::regclass)'
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'plataforma'
  });
};
