'use strict'
/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('json_empleado', {
    json_empleado_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    empleado_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    integra_empleado_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    json_servicio: {
      type: DataTypes.JSON,
      allowNull: true
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    schema: 'integra',
    timestamps: false,

    tableName: 'json_empleado'

  });
};
