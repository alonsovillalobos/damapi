'use strict'

var SequelizeAuto = require('sequelize-auto')
var auto = new SequelizeAuto('forapi2.0', 'postgres', 'P0stgr3s');

auto.run(function (err) {
  if (err) throw err;

  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});

