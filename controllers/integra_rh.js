'use strict'


var Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://postgres:P0stgr3s@10.0.2.37:5432/forapi2.0');

const json_empleado = sequelize.import('../models_integra/json_empleado')
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


// first define the model
const Task = sequelize.define('task', {
  title: Sequelize.STRING,
  rating: { type: Sequelize.STRING, defaultValue: 3 }
})

// now instantiate an object
const task = Task.build({title: 'Task!'})

task.title  // ==> 'very important task'
task.rating // ==> 3



function getModel(req,res){
	var emp = json_empleado.findAll().then(emps => {
	  res.status(200).send({ emps:emps })
	})
	
						
};
/*
 [
   'id_forapi' => 5,
   'movimiento_id' => 3,
   'realizado' => true,
   'id_integra' => 5,
   'datos_empleado' => [
       'nombre' => 'Elizabeth Arias Villegas',
       'centro' => '005ALJU1',
       'empresa' => 'Fundación Proacceso A.C',
       'activo' => true
   ],
   'marca' => [
       'id' => 5,
       'nombre' => 'BD'
   ],
   'accesos' => [
       [
           'plataforma' => 'Control escolar',
           'perfil' => 'Gerente de gestion'
       ], 
       [
           'palataforma'=>'forapi',
           'perfil'=>'Administrador'
       ],
       [
           'palataforma'=>'gis',
           'perfil'=>'Coordinador'
       ]
   ]
];
*/
function buildModel(req,res){
	/*console.log(req.body)
	res.status(200).send( req.body)		*/
	//var params 	= req.body;

	var input   = req.body
	var json_to_str = JSON.stringify(req.body)

	var emp = json_empleado.build({
		json_servicio:json_to_str,
		integra_empleado_id:input.Numero_empleado	
	})
	emp.save().then((stored)=>{
		if(stored){					
			var response_json={
				id_forapi:stored.json_empleado_id
				,movimiento_id:{id:1,accion:"Alta"}
				,realizado:true
				,id_integra:input.integra_empleado_id
				,datos_empleado:
					{
						nombre:input.Nombre_completo
						,centro:input.CentroTrabajoActual
						,empresa:input.EmpresaContrato
						,activo:input.Estatus_empleado

					}
				,marca:
					{
						id:2
						,nombre:"BD"
					}
				,accesos:[
									{
										plataforma : 'Control escolar',
										perfil : 'Gerente de gestion'
									}, 
								    {
								        palataforma:'forapi',
								        perfil:'Administrador'
								    },
								    {
								        palataforma:"gis",
								        perfil:"Coordinador"
									}
						]
				
			}

			res.status(200).send( response_json)		
		}else{
			var stored=input
			var response_json={
				id_forapi:stored.json_empleado_id
				,movimiento_id:{id:1,accion:"Alta"}
				,realizado:false
				,id_integra:input.integra_empleado_id
				,datos_empleado:
					{
						nombre:input.Nombre_completo
						,centro:input.CentroTrabajoActual
						,empresa:input.EmpresaContrato
						,activo:input.Estatus_empleado

					}
				,marca:
					{
						id:2
						,nombre:"BD"
					}
				,accesos:[]
				
			}
			res.status(200).send( response_json)	
		}
	})
	
						
};

module.exports = {
	getModel,
	buildModel
};