'use strict'

var express 		= require('express');
var bodyParser  	= require('body-parser');

var app 			= express();
var reverse_routes 	= require('./routes/reverse');
var integra_routes 	= require('./routes/integra');
/*var options = {
  //inflate: true,
  type: 'application/json'
};*/
//app.use(bodyParser.raw(options));

//app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())

//configuración de cabeceras
app.use((req,res,next)=>{
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','Authorization,X-API-KEY,Origin,X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods','GET,POST,OPTIONS,PUT,DELETE');
	res.header('Allow','GET,POST,OPTIONS,PUT,DELETE');
	next();
})


// carga de rutas base
app.get('/',function(req,res){
 	res.status(200).send({damapi:"damapi"})
})
app.use('/reverse/', reverse_routes );

app.use('/integra/', integra_routes );

module.exports  	= app;

