/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rel_servicio_webarg', {
    servicio_webarg_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    servicio_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_servicio',
        key: 'servicio_id'
      }
    },
    webarg_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_webarg',
        key: 'webarg_id'
      }
    }
  }, {
    tableName: 'rel_servicio_webarg'
  });
};
