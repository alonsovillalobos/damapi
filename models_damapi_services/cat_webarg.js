/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_webarg', {
    webarg_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false
    },
    dtype: {
      type: DataTypes.STRING,
      allowNull: false
    },
    regex: {
      type: DataTypes.STRING,
      allowNull: false
    },
    validation: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    opcional: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }
  }, {
    tableName: 'cat_webarg'
  });
};
