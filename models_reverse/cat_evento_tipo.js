/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_evento_tipo', {
    evento_tipo_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_evento_tipo_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'cat_proyecto',
        key: 'proyecto_id'
      }
    },
    ocultar: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    ana: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    clave: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'cat_evento_tipo'
  });
};
