/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tra_evento', {
    evento_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'cat_proyecto',
        key: 'proyecto_id'
      }
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    socio_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'socio',
        key: 'id'
      }
    },
    correo_electronico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    institucion_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_institucion',
        key: 'institucion_id'
      }
    },
    evento_tipo_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_evento_tipo',
        key: 'evento_tipo_id'
      }
    },
    asistentes: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    lada: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    telefono: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    fecha_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hora: {
      type: DataTypes.STRING,
      allowNull: true
    },
    comentario: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.tra_evento_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: true
    },
    apepat: {
      type: DataTypes.STRING,
      allowNull: true
    },
    apemat: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_reales: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    celular: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    institucion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    otro_perfil: {
      type: DataTypes.STRING,
      allowNull: true
    },
    confirmar: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    evento_estatus_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '1',
      references: {
        model: 'cat_evento_estatus',
        key: 'evento_estatus_id'
      }
    },
    fecha_fin: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hora_fin: {
      type: DataTypes.STRING,
      allowNull: true
    },
    evento_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_evento_categoria',
        key: 'evento_categoria_id'
      }
    },
    nombre_evento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    adjunto: {
      type: DataTypes.STRING,
      allowNull: true
    },
    programa: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    usuario_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    evento_sub_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    motivo_no_realizado: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'tra_evento'
  });
};
