/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_hora', {
    hora_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_hora_hora_id_seq::regclass)'
    },
    hora1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hora2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hora3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hora4: {
      type: DataTypes.STRING,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_hora_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'cat_hora'
  });
};
