/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_centro_horario', {
    cat_centro_horario_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_centro_horario_cat_centro_horario_id_seq::regclass)'
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'cat_proyecto',
        key: 'proyecto_id'
      }
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    telefono: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    hr_lv: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hr_lv_ini: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hr_lv_fin: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hr_s: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hr_s_ini: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hr_s_fin: {
      type: DataTypes.TIME,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_centro_horario_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'cat_centro_horario'
  });
};
