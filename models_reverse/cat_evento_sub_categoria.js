/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_evento_sub_categoria', {
    evento_sub_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    numero: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    evento_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    evento_tipo_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'cat_proyecto',
        key: 'proyecto_id'
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_evento_sub_categoria_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'cat_evento_sub_categoria'
  });
};
