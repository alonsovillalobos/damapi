/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rel_evento_perfil', {
    evento_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tra_evento',
        key: 'evento_id'
      },
      unique: true
    },
    evento_perfil_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'cat_evento_perfil',
        key: 'evento_perfil_id'
      }
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.rel_evento_perfil_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'rel_evento_perfil'
  });
};
