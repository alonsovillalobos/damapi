/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tra_evento_2015', {
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '4'
    },
    proyecto: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    clave: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nombre_evento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    evento_tipo_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    tipo_evento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    evento_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    categoria_evento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    evento_sub_categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    subcategoria_evento: {
      type: DataTypes.STRING,
      allowNull: true
    },
    descripción: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nombre_expositor_invitado: {
      type: DataTypes.STRING,
      allowNull: true
    },
    correo_electrónico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    institución_expositor_invitado: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_esperados: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fecha_inicio: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hora_inicio: {
      type: DataTypes.TIME,
      allowNull: true
    },
    fecha_fin: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hora_fin: {
      type: DataTypes.TIME,
      allowNull: true
    },
    estatus: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_reales: {
      type: DataTypes.STRING,
      allowNull: true
    },
    liga_evidencias: {
      type: DataTypes.STRING,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.tra_evento_2015_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'tra_evento_2015'
  });
};
