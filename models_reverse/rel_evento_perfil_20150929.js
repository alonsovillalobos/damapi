/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rel_evento_perfil_20150929', {
    evento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    evento_perfil_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: true
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: true
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    tableName: 'rel_evento_perfil_20150929'
  });
};
