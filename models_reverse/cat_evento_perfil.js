/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cat_evento_perfil', {
    evento_perfil_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.cat_evento_perfil_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    }
  }, {
    tableName: 'cat_evento_perfil'
  });
};
