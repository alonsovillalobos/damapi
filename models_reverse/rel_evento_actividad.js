/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rel_evento_actividad', {
    evento_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    categoria_curso_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    curso_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    grupo_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 'nextval(evento.rel_evento_actividad_idregcambio_seq::regclass)'
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.fn('now')
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'current_user"()'
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'rel_evento_actividad'
  });
};
