/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('robotix', {
    evento_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    socio_id: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    correo_electronico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    institucion_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    evento_tipo_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    asistentes: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    lada: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    telefono: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    fecha_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hora: {
      type: DataTypes.STRING,
      allowNull: true
    },
    comentario: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    idregcambio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    fecha_alta: {
      type: DataTypes.TIME,
      allowNull: true
    },
    usuario_alta: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fecha_modifico: {
      type: DataTypes.TIME,
      allowNull: true
    },
    usuario_modifico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: true
    },
    apepat: {
      type: DataTypes.STRING,
      allowNull: true
    },
    apemat: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_reales: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    celular: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    institucion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    otro_perfil: {
      type: DataTypes.STRING,
      allowNull: true
    },
    confirmar: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    evento_estatus_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    fecha_fin: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'robotix'
  });
};
