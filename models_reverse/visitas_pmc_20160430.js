/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('visitas_pmc_20160430', {
    centroid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    mes: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro: {
      type: DataTypes.STRING,
      allowNull: true
    },
    responsable_visita: {
      type: DataTypes.STRING,
      allowNull: true
    },
    correo_electrónico: {
      type: DataTypes.STRING,
      allowNull: true
    },
    institucion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lada: {
      type: DataTypes.STRING,
      allowNull: true
    },
    telefono: {
      type: DataTypes.STRING,
      allowNull: true
    },
    celular: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_esperados: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fecha_visita: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hora_visita: {
      type: DataTypes.STRING,
      allowNull: true
    },
    asistentes_registrados: {
      type: DataTypes.STRING,
      allowNull: true
    },
    perfil_asistentes: {
      type: DataTypes.STRING,
      allowNull: true
    },
    actividades: {
      type: DataTypes.STRING,
      allowNull: true
    },
    otras_actividades: {
      type: DataTypes.STRING,
      allowNull: true
    },
    observaciones: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro_ok: {
      type: DataTypes.STRING,
      allowNull: true
    },
    centro_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    proyecto_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    tableName: 'visitas_pmc_20160430'
  });
};
