'use strict'

var express 		= require('express');
var ReverseController 	= require('../controllers/reverse');

var api	 			= express.Router();



api.get('/execute/'  		, ReverseController.ReverseEngineering);

module.exports 		= api;
