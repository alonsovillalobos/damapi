'use strict'

var express 		= require('express');
var IntegraController 	= require('../controllers/integra_rh');
var api	 			= express.Router();



api.get('/model/'  		, IntegraController.getModel);
api.post('/forapi/'  		, IntegraController.buildModel);

module.exports 		= api;
